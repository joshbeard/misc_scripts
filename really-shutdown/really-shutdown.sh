#!/bin/sh
#######################################################################
# really-shutdown
# Last Modified: Wed 08 May 2013 09:18:22 AM MDT
# Ensure the user is serious about a shutdown by confirming the
# hostname and logging a reason
#######################################################################

# If set to >0, prompt for and log the reason for the shutdown/reboot
PROMPT_FOR_REASON=1

LOGFILE="/var/log/shutdown.log"
HOSTNAME="$(hostname -s)"

# Paths to commands
SHUTDOWN="/sbin/shutdown"
DATE="/bin/date"
WHOAMI="/usr/bin/whoami"
TOUCH="/usr/bin/touch"


# Some color codes
txtred='\033[0;31m' # Red
txtylw='\033[0;33m' # Yellow
txtcyn='\033[0;36m' # Cyan
txtrst='\033[0m'    # Text Reset


# Create the log file first
if [ ! -f "${LOGFILE}" ]; then
	${TOUCH} "${LOGFILE}"
fi

# If no arguments were passed, show the shutdown help
if [ -z "$1" ]; then
	${SHUTDOWN} -h
	exit 1
fi

# Prompt the user
printf "\n${txtred}###########################################################################\n"
printf "${txtylw}WARNING:${txtrst} you're about to shutdown the system ${txtcyn}${HOSTNAME}${txtrst}\n\n"
printf "${txtylw}>> ${txtrst}Please enter the hostname of this system: ${txtcyn}"
read inputhostname

# Check if the hostname matches the user input
if [ "$inputhostname" != "$HOSTNAME" ]; then
	printf "${txtrst}FAILED: hostname mismatch.\n"
	exit 1
fi


if [ $PROMPT_FOR_REASON -ne 0 ]; then
	# Prompt for a shutdown reason
	printf "${txtylw}>> ${txtrst}Reason for shutdown: "
	read reason

	# Log the shutdown reason
	echo "$(${DATE} +%Y-%m-%d\ %H:%M) by $(${WHOAMI}): $reason" >> "${LOGFILE}"
fi

# Pass the original arguments to shutdown
${SHUTDOWN} $@
